#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.filters.en_stemming_filter import StemmingFilter


class StemmingFilterTest(unittest.TestCase):

    def test_stemming_filter(self):

        self.stemming = StemmingFilter()

        self.assertEqual(self.stemming.filter(["apples", "apple", "cc"]), ["apple", "apple", "cc"])
        self.assertEqual(self.stemming.filter(["go", "goes", "went", "gone", "going"]), ["go", "go", "go", "go", "go"])
        self.assertEqual(self.stemming.filter(["I'm", "working", "at", "home"]), ["I'm", "work", "at", "home"])
        self.assertEqual(self.stemming.filter(["He", "badly", "want", "to", "eat", "egg"]), ["He", "badly", "want", "to", "eat", "egg"])        
        self.assertEqual(self.stemming.filter(["she", "has", "been", "there"]), ["she", "have", "be", "there"])
        #self.assertEqual(self.stemming.filter(["eat", "ate", "eaten", "eating"]), ["eat", "eat", "eat", "eat"])



if __name__ == '__main__':
    unittest.main()
