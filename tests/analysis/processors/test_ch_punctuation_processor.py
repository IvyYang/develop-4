#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.processors.ch_punctuation_processor import PunctuationProcessor


class PunctuationProcessorTest(unittest.TestCase):

    def test_punctuation_processor(self):

        self.chpp = PunctuationProcessor()

        self.assertEqual(self.chpp.process("中文"), "中文")
        self.assertEqual(self.chpp.process("：中文；"), "中文")
        self.assertEqual(self.chpp.process("中，文。"), "中文")
        self.assertEqual(self.chpp.process("｛中文｝"), "中文")
        self.assertEqual(self.chpp.process("「中文」"), "中文")
        self.assertEqual(self.chpp.process("！？｡＂＃＄％＆＇（）＊＋，－／：；＜＝＞＠［＼］＾＿｀｛｜｝～｟｠｢｣､、〃》「」『』【】〔〕〖〗〘〙〚〛〜〝〞〟〰〾〿–—‘’‛“”„‟…‧﹏."), "")
        self.assertEqual(self.chpp.process("中文。？、~@#￥０"), "中文")
        self.assertEqual(self.chpp.process("。测试简体字？、"), "测试简体字")
        self.assertEqual(self.chpp.process("含有abc，中文。＠"), "含有abc中文")
        self.assertEqual(self.chpp.process("。全型ＡＢＣ１２３不可？"), "全型不可")


if __name__ == '__main__':
    unittest.main()
