#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#
import nltk
from cklabidx.analysis.filters.base_filter import Filter
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet


class StemmingFilter(Filter):
    """
    StemmingFilter
    """

    @staticmethod
    def get_wordnet_pos(treebank_tag):
        if treebank_tag.startswith('J'):
            return wordnet.ADJ
        elif treebank_tag.startswith('V'):
            return wordnet.VERB
        elif treebank_tag.startswith('N'):
            return wordnet.NOUN
        elif treebank_tag.startswith('R'):
            return wordnet.ADV
        else:
            return None
            
    @staticmethod
    def filter(token_list):
        """
        Filter all tokens to all original case
        :param token_list: List of Tokens
        :return: Original token list
        """

        tokens = []
        lemmatizer = WordNetLemmatizer()
        
        for vocabulary, pos in pos_tag((token_list)):
            wordnet_pos = StemmingFilter.get_wordnet_pos(pos) or wordnet.NOUN
            tokens.append(lemmatizer.lemmatize(vocabulary, pos=wordnet_pos))

        return tokens