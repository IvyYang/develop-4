#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from abc import ABCMeta, abstractmethod


class Processor(metaclass=ABCMeta):
    """
    Base Processor
    """

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def process(input_text):
        """
        Null Processor
        :param input_text: Input text to be process
        :return: Processed text
        """
        return input_text
