#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from abc import ABCMeta, abstractmethod

from cklabidx.analysis.analyzer import Analyzer


class BaseIndex(metaclass=ABCMeta):
    """
    Base Index object. Shared functions for all index.
    """
    def __init__(self):
        self.analyzer = Analyzer()

    def set_analyzer(self, analyzer):
        self.analyzer = analyzer

    class IndexException(Exception):
        def __init__(self, msg=""):
            self.msg = msg

        def __str__(self):
            return repr(self.msg)

    class DocNotFoundException(IndexException):
        def __init__(self, msg=""):
            super().__init__(msg)

    class FieldNotFoundException(IndexException):
        def __init__(self, msg=""):
            super().__init__(msg)

    class TermNotFoundException(IndexException):
        def __init__(self, msg=""):
            super().__init__(msg)

    @abstractmethod
    def add_document(self, doc_id, content_dict):
        pass

    @abstractmethod
    def del_document(self, doc_id):
        pass

    @abstractmethod
    def update_document(self, doc_id, content_dict):
        pass

    @abstractmethod
    def save(self, path, index_name):
        pass

    @abstractmethod
    def load(self, path, index_name):
        pass

    @abstractmethod
    def search(self, field, term):
        pass


class InvertedTableIndex(BaseIndex, metaclass=ABCMeta):
    """
    Base Inverted Table Index
    """

    @abstractmethod
    def get_term_tf(self, doc, field, term):
        pass

    @abstractmethod
    def get_term_idf(self, field, term):
        pass

    @abstractmethod
    def get_term_counts(self, doc, field, term):
        pass

    @abstractmethod
    def get_total_term_counts(self, doc, field):
        pass

    @abstractmethod
    def get_sum_total_term_counts(self, field):
        pass

    @abstractmethod
    def get_term_counts_in_all_documents(self, field, term):
        pass

    @abstractmethod
    def get_length_norm_of_doc(self, doc, field):
        pass

    @abstractmethod
    def get_doc_counts_contains_term(self, field, term):
        pass

    @abstractmethod
    def get_doc_term_vector(self, doc, field):
        pass

    @abstractmethod
    def get_unique_doc_counts(self):
        pass

    @abstractmethod
    def get_unique_field_counts(self):
        pass

    @abstractmethod
    def get_unique_term_counts(self):
        pass


class SuffixArrayIndex(BaseIndex, metaclass=ABCMeta):
    """
    Base Suffix Array Index
    """

    @abstractmethod
    def get_term_counts(self):
        pass

    @abstractmethod
    def get_term_position(self):
        pass

    @abstractmethod
    def get_next_term(self):
        pass

    @abstractmethod
    def get_prev_term(self):
        pass

    @abstractmethod
    def get_text_by_offset(self):
        pass
